#ifndef RIGIDBODYASPECT_FIXEDBODY_H
#define RIGIDBODYASPECT_FIXEDBODY_H

#include "../types.h"

namespace rigidbodyaspect
{

  class FixedBody : public GM2SpaceObjectType  {
  public:
    // members
    Qt3DCore::QNodeId m_env_id;
    Unit_Type         m_friction;
    Unit_Type         m_restitution;
    bool              m_deleteCollidingObject;
    GM2Vector         m_normal;
    GM2Vector         m_origin;
  };

}   // namespace rigidbodyaspect

#endif // RIGIDBODYASPECT_FIXEDBODY_H
