#ifndef RIGIDBODYASPECT_RIGIDBODYCONTAINER_H
#define RIGIDBODYASPECT_RIGIDBODYCONTAINER_H

#include "rbtypes.h"

namespace rigidbodyaspect
{

  class RigidBodyContainer {
  public:
    using SphereQHash     = QHash<Qt3DCore::QNodeId, rbtypes::Sphere>;
    using FixedCylinderQHash = QHash<Qt3DCore::QNodeId, rbtypes::FixedCylinder>;
    using FixedPlaneQHash = QHash<Qt3DCore::QNodeId, rbtypes::FixedPlane>;
    using LineQHash = QHash<Qt3DCore::QNodeId, rbtypes::Line>;

    RigidBodyContainer();

    // Sphere
    void                  constructSphere(Qt3DCore::QNodeId id);
    void                  destroySphere(Qt3DCore::QNodeId id);
    rbtypes::Sphere&      sphere(Qt3DCore::QNodeId id);
    const rbtypes::Sphere sphere(Qt3DCore::QNodeId id) const;
    SphereQHash&          spheres();
    const SphereQHash&    spheres() const;

    // Cylinder
    void constructCylinder(Qt3DCore::QNodeId id);
    void destroyCylinder(Qt3DCore::QNodeId id);
    rbtypes::FixedCylinder& cylinder(Qt3DCore::QNodeId id);
    const rbtypes::FixedCylinder cylinder(Qt3DCore::QNodeId id) const;
    FixedCylinderQHash& cylinders();
    const FixedCylinderQHash& cylinders() const;

    // Plane
    void                      constructFixedPlane(Qt3DCore::QNodeId id);
    void                      destroyFixedPlane(Qt3DCore::QNodeId id);
    rbtypes::FixedPlane&      fixedPlane(Qt3DCore::QNodeId id);
    const rbtypes::FixedPlane fixedPlane(Qt3DCore::QNodeId id) const;
    FixedPlaneQHash&          fixedPlanes();
    const FixedPlaneQHash&    fixedPlanes() const;

    // Line
    void constructLine(Qt3DCore::QNodeId id);
    void destroyLine(Qt3DCore::QNodeId id);
    rbtypes::Line& line(Qt3DCore::QNodeId id);
    const rbtypes::Line line(Qt3DCore::QNodeId id) const;
    LineQHash& lines();
    const LineQHash& lines() const;

  private:
    SphereQHash     m_spheres;
    FixedCylinderQHash m_fixed_cylinders;
    FixedPlaneQHash m_fixed_planes;
    LineQHash       m_lines;
  };

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_RIGIDBODYCONTAINER_H
