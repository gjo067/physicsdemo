#ifndef RIGIDBODYASPECT_RBTYPES_H
#define RIGIDBODYASPECT_RBTYPES_H

#include "fixedbody.h"
#include "movingbody.h"

namespace rigidbodyaspect::rbtypes {

    using PhysicsBody = gmlib2::parametric::PSurface<FixedBody>;
    using Sphere     = gmlib2::parametric::PSphere<MovingBody>;
    using FixedSphere = gmlib2::parametric::PSphere<FixedBody>;
    using FixedCylinder = gmlib2::parametric::PCylinder<FixedBody>;
    using FixedPlane = gmlib2::parametric::PPlane<FixedBody>;
    using Line = gmlib2::parametric::PLine<FixedBody>;

}



#endif // RIGIDBODYASPECT_RBTYPES_H
