#ifndef RIGIDBODYASPECT_MOVINGBODY_H
#define RIGIDBODYASPECT_MOVINGBODY_H

#include "../types.h"
#include "fixedbody.h"
#include <unordered_set>

namespace rigidbodyaspect
{

  enum class SphereStates {FreeState, SlidingState, AtRestState, MarkForDeletion};

  class MovingBody : public GM2SpaceObjectType  {
  public:

    // members
    GM2Vector            m_velocity;
    GM2Vector            m_ds;
    seconds_type         m_tc;
    Qt3DCore::QNodeId    m_env_id;
    Qt3DCore::QNodeId    m_id;
    Unit_Type            m_mass;
    Unit_Type            m_friction;
    SphereStates         m_state{SphereStates::FreeState};
    gmlib2::parametric::PSphere<MovingBody>* m_attachedSphere{nullptr};
    std::unordered_set<gmlib2::parametric::PPlane<FixedBody>*> m_attachedPlanes{};
  };

}   // namespace rigidbodyaspect

#endif // RIGIDBODYASPECT_MOVINGBODY_H

