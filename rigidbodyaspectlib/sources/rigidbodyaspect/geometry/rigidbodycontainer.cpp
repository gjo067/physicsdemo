#include "rigidbodycontainer.h"

#include "../utils.h"

namespace rigidbodyaspect
{

  RigidBodyContainer::RigidBodyContainer() {}

  void RigidBodyContainer::constructSphere(Qt3DCore::QNodeId id)
  {
    //    if(m_spheres.contains(id)) throw std::runtime_exception{};
    m_spheres.insert(id, {});
  }

  void RigidBodyContainer::destroySphere(Qt3DCore::QNodeId id)
  {
    //    if(not m_spheres.contains(id)) throw std::runtime_exception{};
    m_spheres.remove(id);
  }

  rbtypes::Sphere& RigidBodyContainer::sphere(Qt3DCore::QNodeId id)
  {
    //    if(not m_spheres.contains(id)) throw std::runtime_exception{};
    return m_spheres[id];
  }

  const rbtypes::Sphere RigidBodyContainer::sphere(Qt3DCore::QNodeId id) const
  {
    //    if(not m_spheres.contains(id)) throw std::runtime_exception{};
    return m_spheres[id];
  }

  RigidBodyContainer::SphereQHash& RigidBodyContainer::spheres()
  {
    return m_spheres;
  }

  const RigidBodyContainer::SphereQHash& RigidBodyContainer::spheres() const
  {
    return m_spheres;
  }

  void RigidBodyContainer::constructCylinder(Qt3DCore::QNodeId id)
  {
    //    if(m_cylinders.contains(id)) throw std::runtime_exception{};
    m_fixed_cylinders.insert(id, {});
  }

  void RigidBodyContainer::destroyCylinder(Qt3DCore::QNodeId id)
  {
    //    if(not m_cs.contains(id)) throw std::runtime_exception{};
    m_fixed_cylinders.remove(id);
  }

  rbtypes::FixedCylinder& RigidBodyContainer::cylinder(Qt3DCore::QNodeId id)
  {
    //    if(not m_cyls.contains(id)) throw std::runtime_exception{};
    return m_fixed_cylinders[id];
  }

  const rbtypes::FixedCylinder RigidBodyContainer::cylinder(Qt3DCore::QNodeId id) const
  {
    //    if(not m_cyl.contains(id)) throw std::runtime_exception{};
    return m_fixed_cylinders[id];
  }

  RigidBodyContainer::FixedCylinderQHash& RigidBodyContainer::cylinders()
  {
    return m_fixed_cylinders;
  }

  const RigidBodyContainer::FixedCylinderQHash& RigidBodyContainer::cylinders() const
  {
    return m_fixed_cylinders;
  }

  void RigidBodyContainer::constructFixedPlane(Qt3DCore::QNodeId id)
  {
    //    if(m_fixed_planes.contains(id)) throw std::runtime_exception{};
    m_fixed_planes.insert(id, {});
  }

  void RigidBodyContainer::destroyFixedPlane(Qt3DCore::QNodeId id)
  {
    //    if(not m_fixed_planes.contains(id)) throw std::runtime_exception{};
    m_fixed_planes.remove(id);
  }

  rbtypes::FixedPlane& RigidBodyContainer::fixedPlane(Qt3DCore::QNodeId id)
  {
    //    if(not m_fixed_planes.contains(id)) throw std::runtime_exception{};
    return m_fixed_planes[id];
  }

  const rbtypes::FixedPlane RigidBodyContainer::fixedPlane(Qt3DCore::QNodeId id) const
  {
    //    if(not m_fixed_planes.contains(id)) throw std::runtime_exception{};
    return m_fixed_planes[id];
  }

  RigidBodyContainer::FixedPlaneQHash& RigidBodyContainer::fixedPlanes()
  {
    return m_fixed_planes;
  }

  const RigidBodyContainer::FixedPlaneQHash& RigidBodyContainer::fixedPlanes() const
  {
    return m_fixed_planes;
  }

  void RigidBodyContainer::constructLine(Qt3DCore::QNodeId id) {
      m_lines.insert(id, {});
  }

  void RigidBodyContainer::destroyLine(Qt3DCore::QNodeId id) {
    m_lines.remove(id);
  }

  rbtypes::Line& RigidBodyContainer::line(Qt3DCore::QNodeId id) {
      return m_lines[id];
  }

  const rbtypes::Line RigidBodyContainer::line(Qt3DCore::QNodeId id) const {
      return m_lines[id];
  }

  RigidBodyContainer::LineQHash& RigidBodyContainer::lines() {
      return m_lines;
  }

  const RigidBodyContainer::LineQHash& RigidBodyContainer::lines() const {
      return m_lines;
  }

}   // namespace rigidbodyaspect
