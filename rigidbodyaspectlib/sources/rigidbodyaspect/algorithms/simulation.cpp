#include "simulation.h"

namespace rigidbodyaspect::algorithms::simulation
{

  void simulateGenericMovingBody(MovingBody& body, seconds_type dt_sec,
                                 const GM2Vector& F)
  {
    const auto dt = dt_sec.count();

    // compute dynamics
    const auto a = F * dt;
    const auto ds = (body.m_velocity + 0.5 * a) * dt;

    // update physical properties
    body.translateLocal(ds);
    body.m_velocity += a;
  }

  void simulateMovingBody(MovingBody &body)
  {
      body.translateLocal(body.m_ds);
  }

  void simulateDsByFactor(MovingBody& body, double ds_factor) {
    body.translateLocal(body.m_ds * ds_factor);
  }

}   // namespace rigidbodyaspect::algorithms::dynamics
