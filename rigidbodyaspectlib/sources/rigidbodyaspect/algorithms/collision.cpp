#include "collision.h"
#include "rigidbodyaspect/constants.h"

#include <set>

namespace rigidbodyaspect::algorithms::collision
{

  detail::CollisionObject detectCollision(Sphere& sphere, Plane& plane, seconds_type dt)
  {
      if(sphere.m_state == SphereStates::SlidingState
              && sphere.m_attachedPlanes.find(&plane) != sphere.m_attachedPlanes.end()) {
//          std::cout << "No collision" << std::endl;
          return {detail::ColliderStatus::NoCollision};
      }

      const auto q = plane.m_origin;
      const auto n = plane.m_normal;

      const auto p = sphere.frameOriginParent();
      const auto r = sphere.m_radius;

      const auto d = (q + r * n) - p;
      const auto ds = sphere.m_ds;

      const auto Q = blaze::inner(d, n);
      const auto R = blaze::inner(ds, n);

      // Moving parallel to plane
      if(std::abs(R) < constants::epsilon) {
          // Touching the plane
          if(std::abs(Q) < constants::epsilon) {
              sphere.m_state = SphereStates::SlidingState;
              sphere.m_attachedPlanes.insert(&plane);
              return {detail::ColliderStatus::NoCollision};
          }
          return {detail::ColliderStatus::NoCollision};
      }

      const auto x = Q / R;

      if(x > 0 && x <= 1) {
          return {detail::ColliderStatus::Collision, &sphere,
                      &plane, false, x, sphere.m_tc + x * (dt - sphere.m_tc)};
      }
      else {
          return {detail::ColliderStatus::NoCollision};
      }
  }

  detail::CollisionObject detectCollision(Sphere &sphere, Cylinder &cylinder, seconds_type dt)
  {
      const auto q = cylinder.frameOriginParent();
      const auto p = sphere.frameOriginParent();

      const auto pl_eval = cylinder.evaluateParent(Cylinder::PSpacePoint{0.0, 0.0},
                                                   Cylinder::PSizeArray{1UL, 1UL});
      const auto c = blaze::subvector<0UL, 3UL>(pl_eval(1UL, 0UL));
      const auto dd = p - q;
      const auto c_ = blaze::cross(c, dd);

      const auto n = blaze::normalize(blaze::cross(c_, c));


      const auto d = (q + (sphere.m_radius + cylinder.m_radius) * n) - p;
      const auto ds = sphere.m_ds;


      const auto Q = blaze::inner(d, n);
      const auto R = blaze::inner(ds, n);

      if(std::abs(R) < constants::epsilon) {
          return {detail::ColliderStatus::NoCollision};
      }

      if(std::abs(Q) < constants::epsilon) {
          return {detail::ColliderStatus::NoCollision};
      }

      const auto x = Q / R;

      if(x > 0 && x <= 1) {
//          std::cout << "Found sphere cylinder collision" << std::endl;
          return {detail::ColliderStatus::Collision, &sphere,
                      &cylinder, false, x, sphere.m_tc + x * (dt - sphere.m_tc)};
      }
      else {
          return {detail::ColliderStatus::NoCollision};
      }
  }

  detail::CollisionObject detectCollision(Sphere& sphere, Sphere& other_sphere,
                                             seconds_type dt)
  {
    const auto Q = other_sphere.frameOriginParent() - sphere.frameOriginParent();
    const auto R = other_sphere.m_ds - sphere.m_ds;
    const auto r = sphere.m_radius + other_sphere.m_radius;

    const auto QdotR = blaze::inner(Q, R);
    const auto QdotQ = blaze::inner(Q, Q);
    const auto RdotR = blaze::inner(R, R);

    const auto c = (QdotQ - std::pow(r, 2));
    const auto inside = std::pow(QdotR, 2) - RdotR * c;

    if(c < constants::epsilon) {
        return {detail::ColliderStatus::NoCollision};
    }

    if(RdotR < constants::epsilon) {
//        std::cout << "SphereSphere: Sphere moving almost in parallel" << std::endl;
        return {detail::ColliderStatus::NoCollision};
    }

    if(inside < 0) {
//        std::cout << "SphereSphere: Moving orthogonally" << std::endl;
        return {detail::ColliderStatus::NoCollision};
    }

    const auto x = (-QdotR - std::sqrt(std::pow(QdotR, 2)
                    - RdotR * (QdotQ - std::pow(r, 2)))) / RdotR;

    const auto tc_min = std::min(sphere.m_tc, other_sphere.m_tc);
//    const auto xdt = tc_min + x * (dt - tc_min);
//    std::cout << x << std::endl;

    if(x > 0 && x <= 1) {
//        std::cout << "Found sphere-sphere collision" << std::endl;
        return {detail::ColliderStatus::Collision, &sphere,
                    &other_sphere, true, x, tc_min + x * (dt - tc_min)};
    }

    return {detail::ColliderStatus::NoCollision};
  }

  CollisionContainer detectCollisions(seconds_type dt, Sphere& di,
                                      std::vector<Sphere*>::iterator d_start,
                                      std::vector<Sphere*>::iterator d_end,
                                      std::vector<Plane*>& fixed_bodies)
  {
      CollisionContainer collisions{};

      //Detect collisions between di and D
      for(auto dc = d_start; dc != d_end; ++dc) {
          if(&**dc == &di) continue;
//          seconds_type tc_min = std::max(di.m_tc, (**dc).m_tc);
          auto c = detectCollision(di, **dc, dt/* - tc_min*/);
          if(c.m_status == detail::ColliderStatus::Collision)
              collisions.push_back(c);
      }

      // Detect collisions between di and S
//      auto t_r = dt - di.m_tc;
      for(auto& si : fixed_bodies) {
          auto c = detectCollision(di, *si, dt);
          if(c.m_status == detail::ColliderStatus::Collision)
              collisions.push_back(c);
      }

      return collisions;
  }

  CollisionContainer detectCollisions(seconds_type dt, Sphere& di,
                                      std::vector<Cylinder*> fixed_cylinders)
  {
      CollisionContainer collisions{};

      for(auto& si : fixed_cylinders) {
          auto c = detectCollision(di, *si, dt);
          if(c.m_status == detail::ColliderStatus::Collision)
              collisions.push_back(c);
      }

      return collisions;
  }

  CollisionContainer detectNewCollisions(detail::CollisionObject& collision, seconds_type dt,
                                         std::vector<Sphere*>& dynamicObjects, std::vector<Plane*>& fixedObjects,
                                         std::vector<Cylinder*>& fixedCylinders)
  {
      CollisionContainer newCollisions;
      auto obj0 = collision.m_obj0;

      auto detected_collisions = detectCollisions(dt, *obj0, dynamicObjects.begin(),
                                                  dynamicObjects.end(), fixedObjects);
      if(detected_collisions.size()) {
          for(auto& col : detected_collisions) {
              if(col.m_obj1 != collision.m_obj1)
                  newCollisions.push_back(col);
          }
      }

      auto detected_cyl_cols = detectCollisions(dt, *obj0, fixedCylinders);

      if(detected_cyl_cols.size()) {
          for(auto& col : detected_cyl_cols) {
              if(col.m_obj1 != collision.m_obj1)
                  newCollisions.push_back(col);
          }
      }

      if(collision.m_obj1_moving) {
          auto obj1 = std::get<Sphere*>(collision.m_obj1);
          auto detected_collisions_obj1 = detectCollisions(dt, *obj1, dynamicObjects.begin(),
                                                           dynamicObjects.end(), fixedObjects);
          for(auto& col : detected_collisions_obj1) {
              if(col.m_obj1_moving) {
                  if(obj0 != std::get<Sphere*>(col.m_obj1))
                      newCollisions.push_back(col);
              }
          }
      }

      return newCollisions;
  }

  void sortAndMakeUnique(CollisionContainer& collisions) {
      // Sort collision objects
      std::sort(collisions.begin(), collisions.end(),
                [](const auto& a, const auto& b) {
          return a.m_t < b.m_t;
      });

      // Make collision list unique with respect to obj0
      std::set<Sphere*> unique_spheres;
      const auto unique_end =
              std::remove_if(std::begin(collisions), std::end(collisions),
                             [&unique_spheres](const detail::CollisionObject& col_obj) {
          auto* sphere = col_obj.m_obj0;
          if(col_obj.m_obj1_moving) {
              auto* sphere2 = std::get<Sphere*>(col_obj.m_obj1);
              if(unique_spheres.count(sphere) || unique_spheres.count(sphere2)) {
//                  std::cout << "Sphere-sphere collision removed for uniqueness" << std::endl;
                  return true;
              }
              else {
                  unique_spheres.insert(sphere);
                  unique_spheres.insert(sphere2);
                  return false;
              }
          }
          else {
              if(unique_spheres.count(sphere)) {
                  return true;
              }
              else {
                  unique_spheres.insert(sphere);
                  return false;
              }
          }
      });
      collisions.erase(unique_end, std::end(collisions));
  }

}   // namespace rigidbodyaspect::algorithms::collision
