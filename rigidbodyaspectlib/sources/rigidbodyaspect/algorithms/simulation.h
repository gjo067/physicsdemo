#ifndef RIGIDBODYASPECT_ALGORITHMS_SIMULATION_H
#define RIGIDBODYASPECT_ALGORITHMS_SIMULATION_H

#include "../geometry/movingbody.h"

namespace rigidbodyaspect::algorithms::simulation
{

  void simulateGenericMovingBody(MovingBody& body, seconds_type dt,
                                 const GM2Vector& F);
  void simulateMovingBody(MovingBody& body);
  void simulateDsByFactor(MovingBody& body, double ds_factor);

}   // namespace rigidbodyaspect::algorithms::simulation


#endif   // RIGIDBODYASPECT_ALGORITHMS_SIMULATION_H
