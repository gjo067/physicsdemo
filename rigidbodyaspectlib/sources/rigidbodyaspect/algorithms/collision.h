#ifndef RIGIDBODYASPECT_ALGORITHMS_COLLISION_H
#define RIGIDBODYASPECT_ALGORITHMS_COLLISION_H

#include "../types.h"
#include "../geometry/rbtypes.h"
#include "../geometry/rigidbodycontainer.h"

#include <deque>
#include <variant>

namespace rigidbodyaspect::algorithms::collision
{
using Sphere = rbtypes::Sphere;
using Cylinder = rbtypes::FixedCylinder;
using Plane = rbtypes::FixedPlane;

namespace detail
{

    enum class ColliderStatus { NoCollision, Collision };

    struct CollisionObject {
        CollisionObject(ColliderStatus status) : m_status{status} {}
        CollisionObject(ColliderStatus status, Sphere* obj0, Plane* obj1,
                        bool obj1_moving, double x, seconds_type t)
            : m_status{status}, m_obj0{obj0}, m_obj1{obj1}, m_obj1_moving{obj1_moving}, m_x{x}, m_t{t} {}
        CollisionObject(ColliderStatus status, Sphere* obj0, Sphere* obj1,
                        bool obj1_moving, double x, seconds_type t)
            : m_status{status}, m_obj0{obj0}, m_obj1{obj1}, m_obj1_moving{obj1_moving}, m_x{x}, m_t{t} {}
        CollisionObject(ColliderStatus status, Sphere* obj0, Cylinder* obj1,
                        bool obj1_moving, double x, seconds_type t)
            : m_status{status}, m_obj0{obj0}, m_obj1{obj1}, m_obj1_moving{obj1_moving}, m_x{x}, m_t{t} {}

        ColliderStatus m_status;
        Sphere* m_obj0;
        std::variant<Sphere*, Plane*, Cylinder*> m_obj1;

        bool m_obj1_moving;
        double m_x;
        seconds_type m_t;
    };

    }   // namespace detail

    using CollisionContainer = std::deque<detail::CollisionObject>;

    CollisionContainer detectCollisions(seconds_type dt, Sphere& di,
                                        std::vector<Sphere*>::iterator d_start,
                                        std::vector<Sphere*>::iterator d_end,
                                        std::vector<Plane*>& fixed_bodies);
    CollisionContainer detectCollisions(seconds_type dt, Sphere& di,
                                        std::vector<Cylinder*> fixed_cylinders);
    CollisionContainer detectNewCollisions(detail::CollisionObject& collision,
                                           seconds_type dt,
                                           std::vector<Sphere*>& dynamicObjects,
                                           std::vector<Plane*>& fixedObjects,
                                           std::vector<Cylinder*>& fixedCylinders);

    detail::CollisionObject detectCollision(Sphere& sphere,
                                            Plane& plane,
                                            seconds_type dt);
    detail::CollisionObject detectCollision(Sphere& sphere,
                                            Sphere& other_sphere,
                                            seconds_type dt);
    detail::CollisionObject detectCollision(Sphere& sphere,
                                            Cylinder& cylinder,
                                            seconds_type dt);

    void sortAndMakeUnique(CollisionContainer& collisions);

}   // namespace rigidbodyaspect::algorithms::collision


#endif   // RIGIDBODYASPECT_ALGORITHMS_COLLISION_H
