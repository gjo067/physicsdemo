#ifndef UPDATEPROPERTIES_H
#define UPDATEPROPERTIES_H

#include "../geometry/rbtypes.h"
#include "rigidbodyaspect/constants.h"
#include "../backend/environmentbackend.h"

namespace rigidbodyaspect::algorithms::properties
{
    using Sphere = rbtypes::Sphere;
    using Cylinder = rbtypes::FixedCylinder;
    using Plane = rbtypes::FixedPlane;

    GM2Vector getClosestPointOnSurface(Plane& plane, GM2Vector p) {
        Plane::PSpacePoint solution{0, 0};
        blaze::StaticVector<double, 2UL, blaze::columnVector> x{1.0, 1.0};

        while(x[0] > constants::epsilon && x[1] > constants::epsilon) {
            const auto pl_eval = plane.evaluateParent(solution, Plane::PSizeArray{1UL, 1UL});
            const auto Su = blaze::subvector<0UL, 3UL>(pl_eval(1UL, 0UL));
            const auto Sv = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 1UL));
            const auto q = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 0UL));
            const auto d = q - p;

            blaze::StaticMatrix<double, 2UL, 2UL> A{
              {blaze::inner(Su, Su), blaze::inner(Su, Sv)},
              {blaze::inner(Su, Sv), blaze::inner(Sv, Sv)}
            };

            blaze::invert(A);

            blaze::StaticVector<double, 2UL, blaze::columnVector> b{
                -blaze::inner(d, Su),
                -blaze::inner(d, Sv)
            };

            x = A*b;

            solution += x;
        }

        return blaze::subvector<0UL, 3UL>(plane.evaluateParent(solution, Plane::PSizeArray{0UL, 0UL})(0UL, 0UL));
    }

    void correctDsWhileSliding(Sphere& sphere, Plane& plane) {
        // Project ds onto the plane.
        const auto pp = sphere.frameOriginParent() + sphere.m_ds;
        const auto q = getClosestPointOnSurface(plane, pp);
        const auto d = (q - pp) + plane.m_normal * sphere.m_radius;
        sphere.m_ds = sphere.m_ds + d;
    }

    void correctVelocityWhileSliding(Sphere& sphere, Plane& plane) {
        // Project velocity onto the plane.
        const auto pp_v = sphere.frameOriginParent() + sphere.m_velocity;
        const auto q_v = getClosestPointOnSurface(plane, pp_v);
        const auto d_v = (q_v - pp_v) + plane.m_normal * sphere.m_radius;
        sphere.m_velocity = sphere.m_velocity + d_v;
    }

    void computeCacheProperties(const EnvironmentBackend& env_BE, Sphere &sphere, seconds_type tr)
    {
        const auto g = env_BE.m_gravity * tr.count();
        sphere.m_ds = (sphere.m_velocity + 0.5 * g) * tr.count();

        // If sliding, check if ds should be corrected.
        if(sphere.m_state == SphereStates::SlidingState) {
            for(auto* plane : sphere.m_attachedPlanes) {
                const auto x = blaze::inner(sphere.m_ds, plane->m_normal);
                if(x > 0) {
                    sphere.m_attachedPlanes.erase(plane);
                    if(sphere.m_attachedPlanes.empty()) {
                        sphere.m_state = SphereStates::FreeState;
                        return;
                    }
                }
                else if(x < 0) {
                    correctDsWhileSliding(sphere, *plane);
                }

                // Check if the sphere should be at rest
                if(std::abs(blaze::inner(-plane->m_normal * sphere.m_radius, sphere.m_ds)
                            - blaze::inner(sphere.m_ds, sphere.m_ds)) < constants::epsilon) {
                    sphere.m_state = SphereStates::AtRestState;
                    sphere.m_velocity = GM2Vector{0.0, 0.0, 0.0};
                    sphere.m_ds = GM2Vector{0.0, 0.0, 0.0};
                    return;
                }
            }
        }
    }

    void updateVelocity(const EnvironmentBackend &env_BE, Sphere &sphere, seconds_type dt, double x)
    {
        auto a = env_BE.m_gravity;
        if(sphere.m_state == SphereStates::FreeState) {
            sphere.m_velocity = sphere.m_velocity + a * x * dt.count();
        }
        else if(sphere.m_state == SphereStates::SlidingState) {
    //          const auto a = ((x * sphere.m_ds) / (x * dt.count()) - sphere.m_velocity) / (x * dt.count() * 0.5);
    //          sphere.m_velocity = (sphere.m_velocity + a * x * dt.count());
            if(std::abs(blaze::inner(sphere.m_ds, a)) < constants::epsilon)
                a = GM2Vector{0.0, 0.0, 0.0};
            sphere.m_velocity = sphere.m_velocity + a * x * dt.count();

            double friction = 0;
            for(auto* plane : sphere.m_attachedPlanes) {
                friction += plane->m_friction;
            }
            friction /= sphere.m_attachedPlanes.size();
            sphere.m_velocity -= sphere.m_velocity * (1 - friction) * x * dt.count(); // Friction
        }
    }

    void checkState(Sphere& sphere, Plane& plane) {
        const auto d = (plane.m_origin + sphere.m_radius * plane.m_normal) - sphere.frameOriginParent();

        if(std::abs(blaze::inner(d, plane.m_normal)) < constants::epsilon
                && blaze::inner(sphere.m_ds, plane.m_normal) <= 0) {
            if(std::abs(blaze::inner(plane.m_normal * -sphere.m_radius, sphere.m_ds)
                        - blaze::inner(sphere.m_ds, sphere.m_ds)) < constants::epsilon) {
                sphere.m_state = SphereStates::AtRestState;
                sphere.m_attachedPlanes.insert(&plane);
                sphere.m_velocity = GM2Vector{0.0, 0.0, 0.0};
                sphere.m_ds = GM2Vector{0.0, 0.0, 0.0};
            }
            else {
                sphere.m_state = SphereStates::SlidingState;
                sphere.m_attachedPlanes.insert(&plane);
                correctDsWhileSliding(sphere, plane);
//                std::cout << "Attached planes: " << sphere.m_attachedPlanes.size() << std::endl;
            }
        }
    }
}

#endif // UPDATEPROPERTIES_H
