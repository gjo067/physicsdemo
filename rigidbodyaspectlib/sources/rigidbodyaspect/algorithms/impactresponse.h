#ifndef IMPACTRESPONSE_H
#define IMPACTRESPONSE_H

#include "updateproperties.h"
#include "collision.h"
#include "../geometry/rbtypes.h"

namespace rigidbodyaspect::algorithms::impactresponse
{
    using Sphere = rbtypes::Sphere;
    using Cylinder = rbtypes::FixedCylinder;
    using Plane = rbtypes::FixedPlane;

    void computeImpact(Sphere& sphere, const Plane& plane)
    {
        if(sphere.m_state == SphereStates::SlidingState) {
            for(auto* att_plane : sphere.m_attachedPlanes) {
                properties::correctVelocityWhileSliding(sphere, *att_plane);
            }
        }
        const auto n = plane.m_normal;
        const auto v = sphere.m_velocity;

        sphere.m_velocity = (v - 2 * blaze::inner(v, n) * n) * plane.m_restitution;
    }

    void computeImpact(Sphere& sphere, const Cylinder& cylinder)
    {
        if(cylinder.m_deleteCollidingObject) {
            sphere.m_state = SphereStates::MarkForDeletion;
            return;
        }

        if(sphere.m_state == SphereStates::SlidingState) {
            for(auto* att_plane : sphere.m_attachedPlanes) {
                properties::correctVelocityWhileSliding(sphere, *att_plane);
            }
        }

        const auto q = cylinder.frameOriginParent();
        const auto p = sphere.frameOriginParent();

        const auto pl_eval = cylinder.evaluateParent(Cylinder::PSpacePoint{0.0, 0.0},
                                                     Cylinder::PSizeArray{1UL, 1UL});
        const auto c = blaze::subvector<0UL, 3UL>(pl_eval(1UL, 0UL));
        const auto dd = p - q;
        const auto c_ = blaze::cross(c, dd);

        const auto n = blaze::normalize(blaze::cross(c_, c));
        const auto v = sphere.m_velocity;

        sphere.m_velocity = v - 2 * blaze::inner(v, n) * n;
    }

    void computeImpact(Sphere& sphere, Sphere& other_sphere)
    {
        // If the moving sphere is hitting the resting sphere straight from above
        // Compute impact between moving sphere and resting spheres plane.
        if(other_sphere.m_state == SphereStates::AtRestState) {
            if(other_sphere.m_attachedPlanes.size() == 1) {
                const auto plane = **other_sphere.m_attachedPlanes.begin();
                const auto d_ = blaze::normalize((plane.frameOriginParent() + 2 * other_sphere.m_radius * plane.m_normal) - sphere.frameOriginParent());
//                std::cout << blaze::inner(d_, plane.m_normal) << std::endl;
                if(blaze::inner(d_, plane.m_normal) <= -1) {
                    computeImpact(sphere, plane);
                    return;
                }
            }

            other_sphere.m_state = SphereStates::SlidingState;
        }
        else if(sphere.m_state == SphereStates::AtRestState) {
            if(sphere.m_attachedPlanes.size() == 1) {
                const auto plane = **sphere.m_attachedPlanes.begin();
                const auto d_ = blaze::normalize((plane.frameOriginParent() + 2 * sphere.m_radius * plane.m_normal) - other_sphere.frameOriginParent());
//                std::cout << blaze::inner(d_, plane.m_normal) << std::endl;
                if(blaze::inner(d_, plane.m_normal) <= -1) {
                    computeImpact(other_sphere, plane);
                    return;
                }
            }

            sphere.m_state = SphereStates::SlidingState;
        }

        const auto p0 = sphere.frameOriginParent();
        const auto p1 = other_sphere.frameOriginParent();

        const auto d = blaze::normalize(p1 - p0);

        if(sphere.m_state == SphereStates::SlidingState) {
            for(auto* plane : sphere.m_attachedPlanes) {
                properties::correctVelocityWhileSliding(sphere, *plane);
            }
        }
        else if(other_sphere.m_state == SphereStates::SlidingState) {
            for(auto* plane : other_sphere.m_attachedPlanes) {
                properties::correctVelocityWhileSliding(other_sphere, *plane);
            }
        }

        GM2Vector liv_d;

        if(sphere.m_state != SphereStates::FreeState && other_sphere.m_state != SphereStates::FreeState
                && sphere.m_attachedPlanes.size() == 1 && other_sphere.m_attachedPlanes.size() == 1) {
            liv_d = (*sphere.m_attachedPlanes.begin())->m_normal;
        }
        else {
            /*const auto*/ liv_d = gmlib2::algorithms::linearIndependentVector(gmlib2::VectorT<double, 3UL>{d[0], d[1], d[2]});
        }

        const auto n = blaze::cross(liv_d, d) / blaze::length(blaze::cross(liv_d, d));

        const auto v0 = sphere.m_velocity;
        const auto v1 = other_sphere.m_velocity;

        const auto v0d = blaze::inner(v0, d);
        const auto v1d = blaze::inner(v1, d);
        const auto v0n = blaze::inner(v0, n);
        const auto v1n = blaze::inner(v1, n);

        const auto m0 = sphere.m_mass;
        const auto m1 = other_sphere.m_mass;

        const auto v0d_new = (m0 - m1) / (m0 + m1) * v0d + 2 * m1 / (m0 + m1) * v1d;
        const auto v1d_new = (m1 - m0) / (m0 + m1) * v1d + 2 * m0 / (m0 + m1) * v0d;

        sphere.m_velocity = v0n * n + v0d_new * d;
        other_sphere.m_velocity = v1n * n + v1d_new * d;
    }
}

#endif // IMPACTRESPONSE_H
