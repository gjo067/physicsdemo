#include "linecontrollerbackend.h"

#include "rigidbodyaspect.h"
#include "../frontend/linecontroller.h"
#include "../utils.h"

namespace rigidbodyaspect
{

  LineControllerBackend::LineControllerBackend(RigidBodyContainer& rigid_bodies)
    : Qt3DCore::QBackendNode(Qt3DCore::QBackendNode::ReadWrite),
      m_rigid_bodies{rigid_bodies}
  {
  }

  LineControllerBackend::~LineControllerBackend()
  {
    m_rigid_bodies.destroyFixedPlane(peerId());
  }

  void LineControllerBackend::queueFrontendUpdate()
  {

    const auto& hframe = m_rigid_bodies.line(peerId()).pSpaceFrameParent();

    // Convert data to sendable frame
    QMatrix3x3 q_hframe;
    for (auto i = 0UL; i < 3UL; ++i) {
      q_hframe(int(i), 0)
        = float(blaze::submatrix<0UL, 0UL, 3UL, 4UL>(hframe)(i, 0UL));   // dir
      q_hframe(int(i), 1)
        = float(blaze::submatrix<0UL, 0UL, 3UL, 4UL>(hframe)(i, 2UL));   // up
      q_hframe(int(i), 2)
        = float(blaze::submatrix<0UL, 0UL, 3UL, 4UL>(hframe)(i, 3UL));   // pos
    }

    // Send data
    auto e = Qt3DCore::QPropertyUpdatedChangePtr::create(peerId());
    e->setDeliveryFlags(Qt3DCore::QSceneChange::Nodes);
    e->setPropertyName(QByteArrayLiteral("dupframe_BE"));
    e->setValue(QVariant::fromValue(q_hframe));
    notifyObservers(e);
  }

  void LineControllerBackend::setPt(QVector3D pt) {
      if(m_pt == pt) return;

      m_pt = pt;

      if(m_dynamics_type not_eq RigidBodyDynamicsType::DynamicObject) return;

      m_rigid_bodies.line(peerId()).m_pt = utils::qVecToGM2HVec3(pt, true);
  }

  void LineControllerBackend::setV(QVector3D v) {
      if(m_v == v) return;

      m_v = v;

      if(m_dynamics_type != RigidBodyDynamicsType::DynamicObject) return;

      m_rigid_bodies.line(peerId()).m_v = utils::qVecToGM2HVec3(v, false);
  }

  void LineControllerBackend::setDupFrame(const QMatrix3x3 dup_frame)
  {
    if (qFuzzyCompare(dup_frame, m_dup_frame)) return;

    m_dup_frame = dup_frame;

    if (m_dynamics_type not_eq RigidBodyDynamicsType::DynamicObject) return;

    const auto& [dir, up, pos] = utils::dupFrameToDUP(m_dup_frame);
    m_rigid_bodies.line(peerId()).setFrameParent(dir, up, pos);
  }

  void LineControllerBackend::setDynamicsType(
    const RigidBodyDynamicsType& dynamics_type)
  {
    if (m_dynamics_type == dynamics_type) return;
  }

  void LineControllerBackend::setEnvironmentId(const Qt3DCore::QNodeId& id)
  {
    if (m_environment_id == id) return;

    m_environment_id = id;

    if (m_dynamics_type not_eq RigidBodyDynamicsType::DynamicObject) return;

    m_rigid_bodies.line(peerId()).m_env_id = m_environment_id;
  }

  void LineControllerBackend::initializeFromPeer(
    const Qt3DCore::QNodeCreatedChangeBasePtr& change)
  {
    const auto typedChange
      = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<LineInitialData>>(
        change);
    const auto& data = typedChange->data;
    m_dynamics_type  = data.m_dynamics_type;
    m_dup_frame      = data.m_dup_frame;
    m_environment_id = data.m_environment_id;
    m_pt = data.m_pt;
    m_v = data.m_v;

    m_rigid_bodies.constructLine(peerId());
    auto& plane = m_rigid_bodies.line(peerId());

    const auto& [dir, up, pos] = utils::dupFrameToDUP(m_dup_frame);
    plane.setFrameParent(dir, up, pos);
    plane.m_env_id = m_environment_id;
    plane.m_pt = utils::qVecToGM2HVec3(data.m_pt, true);
    plane.m_v = utils::qVecToGM2HVec3(data.m_v, false);
  }

  void
  LineControllerBackend::sceneChangeEvent(const Qt3DCore::QSceneChangePtr& e)
  {
    if (e->type() == Qt3DCore::PropertyUpdated) {
      const auto change
        = qSharedPointerCast<Qt3DCore::QPropertyUpdatedChange>(e);
      if (change->propertyName() == QByteArrayLiteral("dupFrame"))
        setDupFrame(change->value().value<QMatrix3x3>());
      else if (change->propertyName() == QByteArrayLiteral("dynamicsType"))
        setDynamicsType(change->value().value<RigidBodyDynamicsType>());
      else if (change->propertyName() == QByteArrayLiteral("environment"))
        setEnvironmentId(change->value().value<Qt3DCore::QNodeId>());
      else if (change->propertyName() == QByteArrayLiteral("pt"))
          setPt(change->value().value<QVector3D>());
      else if (change->propertyName() == QByteArrayLiteral("v"))
          setV(change->value().value<QVector3D>());
    }

    QBackendNode::sceneChangeEvent(e);
  }

  LineControllerBackendMapper::LineControllerBackendMapper(
    RigidBodyAspect* aspect)
    : m_aspect(aspect)
  {
    Q_ASSERT(m_aspect);
  }

  Qt3DCore::QBackendNode* LineControllerBackendMapper::create(
    const Qt3DCore::QNodeCreatedChangeBasePtr& change) const
  {
    auto backend = new LineControllerBackend(m_aspect->rigidBodies());
    m_aspect->addLineControllerBackend(change->subjectId(), backend);
    return backend;
  }

  Qt3DCore::QBackendNode*
  LineControllerBackendMapper::get(Qt3DCore::QNodeId id) const
  {
    return m_aspect->lineControllerBackend(id);
  }

  void LineControllerBackendMapper::destroy(Qt3DCore::QNodeId id) const
  {
    delete m_aspect->takeLineControllerBackend(id);
  }

}   // namespace rigidbodyaspect
