#include "cylindercontrollerbackend.h"

#include "rigidbodyaspect.h"
#include "../frontend/cylindercontroller.h"
#include "../utils.h"

namespace rigidbodyaspect
{
    CylinderControllerBackend::CylinderControllerBackend(RigidBodyContainer& rigid_bodies)
      : Qt3DCore::QBackendNode(Qt3DCore::QBackendNode::ReadWrite),
        m_rigid_bodies{rigid_bodies}
    {
    }

    CylinderControllerBackend::~CylinderControllerBackend()
    {
      m_rigid_bodies.destroyCylinder(peerId());
    }

    void CylinderControllerBackend::queueFrontendUpdate()
    {
        const auto& hframe = m_rigid_bodies.fixedPlane(peerId()).pSpaceFrameParent();

        // Convert data to sendable frame
        QMatrix3x3 q_hframe;
        for (auto i = 0UL; i < 3UL; ++i) {
          q_hframe(int(i), 0)
            = float(blaze::submatrix<0UL, 0UL, 3UL, 4UL>(hframe)(i, 0UL));   // dir
          q_hframe(int(i), 1)
            = float(blaze::submatrix<0UL, 0UL, 3UL, 4UL>(hframe)(i, 2UL));   // up
          q_hframe(int(i), 2)
            = float(blaze::submatrix<0UL, 0UL, 3UL, 4UL>(hframe)(i, 3UL));   // pos
        }

      // Send data
      auto e = Qt3DCore::QPropertyUpdatedChangePtr::create(peerId());
      e->setDeliveryFlags(Qt3DCore::QSceneChange::Nodes);
      e->setPropertyName(QByteArrayLiteral("dupframe_BE"));
      e->setValue(QVariant::fromValue(q_hframe));
      notifyObservers(e);
    }

    void CylinderControllerBackend::setRadius(float radius)
    {
      if (m_radius == radius) return;

      m_radius = radius;

      if (m_dynamics_type not_eq RigidBodyDynamicsType::DynamicObject) return;

      m_rigid_bodies.cylinder(peerId()).m_radius = double(radius);
    }

    void CylinderControllerBackend::setDeleteCollidingObject(bool deleteCollidingObject)
    {
        if(m_deleteCollidingObject == deleteCollidingObject) return;

        m_deleteCollidingObject = deleteCollidingObject;

        if(m_dynamics_type not_eq RigidBodyDynamicsType::DynamicObject) return;

        m_rigid_bodies.cylinder(peerId()).m_deleteCollidingObject = deleteCollidingObject;
    }

    void CylinderControllerBackend::setHeight(float height)
    {
      if (m_height == height) return;

      m_height = height;

      if (m_dynamics_type not_eq RigidBodyDynamicsType::DynamicObject) return;

      m_rigid_bodies.cylinder(peerId()).m_height = double(height);
    }

    void CylinderControllerBackend::setDupFrame(const QMatrix3x3 dup_frame)
    {
      if (qFuzzyCompare(dup_frame, m_dup_frame)) return;

      m_dup_frame = dup_frame;

      if (m_dynamics_type not_eq RigidBodyDynamicsType::DynamicObject) return;

      const auto& [dir, up, pos] = utils::dupFrameToDUP(m_dup_frame);
      m_rigid_bodies.cylinder(peerId()).setFrameParent(dir, up, pos);
    }

    void CylinderControllerBackend::setDynamicsType(
      const RigidBodyDynamicsType& dynamics_type)
    {
      if (m_dynamics_type == dynamics_type) return;
    }

    void CylinderControllerBackend::setEnvironmentId(const Qt3DCore::QNodeId& id)
    {
      if (m_environment_id == id) return;

      m_environment_id = id;

      if (m_dynamics_type not_eq RigidBodyDynamicsType::DynamicObject) return;

      m_rigid_bodies.cylinder(peerId()).m_env_id = m_environment_id;
    }

      void CylinderControllerBackend::initializeFromPeer(
        const Qt3DCore::QNodeCreatedChangeBasePtr& change)
      {
        const auto typedChange
          = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<CylinderInitialData>>(
            change);
        const auto& data = typedChange->data;
        m_dynamics_type  = data.m_dynamics_type;
        m_dup_frame      = data.m_dup_frame;
        m_radius         = data.m_radius;
        m_height         = data.m_height;
        m_deleteCollidingObject = data.m_deleteCollidingObject;
        m_environment_id = data.m_environment_id;

        m_rigid_bodies.constructCylinder(peerId());
        auto& cylinder = m_rigid_bodies.cylinder(peerId());

        // geometric properties
        const auto& [dir, up, pos] = utils::dupFrameToDUP(m_dup_frame);
        cylinder.setFrameParent(dir, up, pos);
        cylinder.m_radius = double(m_radius);
        cylinder.m_height = double(m_height);
        cylinder.m_deleteCollidingObject = m_deleteCollidingObject;
        cylinder.m_env_id = m_environment_id;
      }

      void
      CylinderControllerBackend::sceneChangeEvent(const Qt3DCore::QSceneChangePtr& e)
      {
        if (e->type() == Qt3DCore::PropertyUpdated) {
          const auto change
            = qSharedPointerCast<Qt3DCore::QPropertyUpdatedChange>(e);
          if (change->propertyName() == QByteArrayLiteral("dupFrame"))
            setDupFrame(change->value().value<QMatrix3x3>());
          else if (change->propertyName() == QByteArrayLiteral("dynamicsType"))
            setDynamicsType(change->value().value<RigidBodyDynamicsType>());
          else if (change->propertyName() == QByteArrayLiteral("environment"))
            setEnvironmentId(change->value().value<Qt3DCore::QNodeId>());
          else if (change->propertyName() == QByteArrayLiteral("radius"))
            setRadius(change->value().toFloat());
          else if (change->propertyName() == QByteArrayLiteral("height"))
              setHeight(change->value().toFloat());
          else if(change->propertyName() == QByteArrayLiteral("deleteCollidingObject"))
              setDeleteCollidingObject(change->value().toBool());
        }

        QBackendNode::sceneChangeEvent(e);
      }

      CylinderControllerBackendMapper::CylinderControllerBackendMapper(
        RigidBodyAspect* aspect)
        : m_aspect(aspect)
      {
        Q_ASSERT(m_aspect);
      }

      Qt3DCore::QBackendNode* CylinderControllerBackendMapper::create(
        const Qt3DCore::QNodeCreatedChangeBasePtr& change) const
      {
        auto backend = new CylinderControllerBackend(m_aspect->rigidBodies());
        m_aspect->addCylinderControllerBackend(change->subjectId(), backend);
        return backend;
      }

      Qt3DCore::QBackendNode*
      CylinderControllerBackendMapper::get(Qt3DCore::QNodeId id) const
      {
        return m_aspect->cylinderControllerBackend(id);
      }

      void CylinderControllerBackendMapper::destroy(Qt3DCore::QNodeId id) const
      {
        delete m_aspect->takeCylinderControllerBackend(id);
      }
}
