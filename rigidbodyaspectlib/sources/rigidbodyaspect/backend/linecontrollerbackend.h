#ifndef RIGIDBODYASPECT_LINECONTROLLERBACKEND_H
#define RIGIDBODYASPECT_LINECONTROLLERBACKEND_H

#include "../types.h"

#include "../geometry/rigidbodycontainer.h"

// qt
#include <QMatrix4x4>
#include <Qt3DCore/QBackendNode>

namespace rigidbodyaspect
{

  class LineControllerBackend : public Qt3DCore::QBackendNode {
  public:
    LineControllerBackend(RigidBodyContainer& rigid_bodies);
    ~LineControllerBackend() override;

    void queueFrontendUpdate();

  private:
    RigidBodyDynamicsType m_dynamics_type;
    QMatrix3x3            m_dup_frame;
    RigidBodyContainer&   m_rigid_bodies;
    Qt3DCore::QNodeId     m_environment_id;
    QVector3D m_pt;
    QVector3D m_v;


    void setDupFrame(const QMatrix3x3 dup_frame);

    void setDynamicsType(const RigidBodyDynamicsType& dynamics_type);
    void setEnvironmentId(const Qt3DCore::QNodeId& id);

    void setPt(QVector3D pt);
    void setV(QVector3D v);


    rbtypes::Line& line();

    // QBackendNode interface
  protected:
    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr& e) override;

  private:
    void initializeFromPeer(
      const Qt3DCore::QNodeCreatedChangeBasePtr& change) override;
  };


  class RigidBodyAspect;

  class LineControllerBackendMapper : public Qt3DCore::QBackendNodeMapper {
  public:
    explicit LineControllerBackendMapper(RigidBodyAspect* aspect);

  private:
    RigidBodyAspect* m_aspect;

    // QBackendNodeMapper interface
  public:
    Qt3DCore::QBackendNode* create(const Qt3DCore::QNodeCreatedChangeBasePtr& change) const override;
    Qt3DCore::QBackendNode* get(Qt3DCore::QNodeId id) const override;
    void                    destroy(Qt3DCore::QNodeId id) const override;
  };

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_LINECONTROLLERBACKEND_H
