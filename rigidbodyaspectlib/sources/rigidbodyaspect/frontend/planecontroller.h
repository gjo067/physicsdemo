#ifndef RIGIDBODYASPECT_PLANECONTROLLER_H
#define RIGIDBODYASPECT_PLANECONTROLLER_H


#include "abstractrigidbodycontroller.h"

namespace rigidbodyaspect
{

  class PlaneController : public AbstractRigidBodyController {
    Q_OBJECT

    Q_PROPERTY(QVector3D pt MEMBER m_pt NOTIFY ptChanged)
    Q_PROPERTY(QVector3D u MEMBER m_u NOTIFY uChanged)
    Q_PROPERTY(QVector3D v MEMBER m_v NOTIFY vChanged)
    Q_PROPERTY(float friction MEMBER m_friction NOTIFY frictionChanged)
    Q_PROPERTY(float restitution MEMBER m_restitution NOTIFY restitutionChanged)

  public:
    PlaneController(Qt3DCore::QNode* parent = nullptr);

    QVector3D m_pt{0.0f, 0.0f, 0.0f};
    QVector3D m_u{10.0f, 0.0f, 0.0f};
    QVector3D m_v{0.0f, 0.0f, -10.0f};
    float m_friction = 1.0f;
    float m_restitution = 1.0f;


  signals:
    void ptChanged(const QVector3D& pt);
    void uChanged(const QVector3D& u);
    void vChanged(const QVector3D& v);
    void frictionChanged(float friction);
    void restitutionChanged(float restitution);

    // QNode interface
  private:
    Qt3DCore::QNodeCreatedChangeBasePtr createNodeCreationChange() const override;
  };


  struct PlaneInitialData : AbstractRigidBodyInitialData {
    QVector3D m_pt;
    QVector3D m_u;
    QVector3D m_v;
    float m_friction;
    float m_restitution;
  };

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_PLANECONTROLLER_H
