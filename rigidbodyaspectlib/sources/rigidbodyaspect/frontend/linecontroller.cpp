#include "linecontroller.h"
#include "rigidbodyaspect/utils.h"

namespace rigidbodyaspect
{

  LineController::LineController(Qt3DCore::QNode* parent)
    : AbstractRigidBodyController(parent)
  {
  }

  Qt3DCore::QNodeCreatedChangeBasePtr
  LineController::createNodeCreationChange() const
  {
    auto creationChange
      = Qt3DCore::QNodeCreatedChangePtr<LineInitialData>::create(this);

    auto& data          = creationChange->data;
    data.m_dynamics_type  = m_dynamics_type;
    data.m_dup_frame      = m_dup_frame;
    data.m_environment_id = Qt3DCore::qIdForNode(m_environment);

    data.m_pt = m_pt;
    data.m_v = m_v;

    return std::move(creationChange);
  }

}   // namespace rigidbodyaspect
