#ifndef RIGIDBODYASPECT_LINECONTROLLER_H
#define RIGIDBODYASPECT_LINECONTROLLER_H

#include "abstractrigidbodycontroller.h"

namespace rigidbodyaspect {

    class LineController : public AbstractRigidBodyController {
        Q_OBJECT

        Q_PROPERTY(QVector3D pt MEMBER m_pt NOTIFY ptChanged)
        Q_PROPERTY(QVector3D v MEMBER m_v NOTIFY vChanged)

    public:
        LineController(Qt3DCore::QNode* parent = nullptr);

        QVector3D m_pt{0.0f, 0.0f, 0.0f};
        QVector3D m_v{1.0f, 1.0f, 1.0f};

    signals:
        void ptChanged(const QVector3D& pt);
        void vChanged(const QVector3D& v);

    private:
        Qt3DCore::QNodeCreatedChangeBasePtr createNodeCreationChange() const override;
    };

    struct LineInitialData : AbstractRigidBodyInitialData {
        QVector3D m_pt;
        QVector3D m_v;
    };
}

#endif // RIGIDBODYASPECT_LINECONTROLLER_H
