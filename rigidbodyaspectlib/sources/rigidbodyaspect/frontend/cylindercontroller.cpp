#include "cylindercontroller.h"

namespace rigidbodyaspect
{
    CylinderController::CylinderController(Qt3DCore::QNode* parent)
        : AbstractRigidBodyController (parent)
    {
    }

    Qt3DCore::QNodeCreatedChangeBasePtr
        CylinderController::createNodeCreationChange() const
    {
        auto creationChange
                = Qt3DCore::QNodeCreatedChangePtr<CylinderInitialData>::create(this);

        auto& data = creationChange->data;
        data.m_dynamics_type = m_dynamics_type;
        data.m_dup_frame = m_dup_frame;
        data.m_environment_id = Qt3DCore::qIdForNode(m_environment);

        data.m_radius = m_radius;
        data.m_height = m_height;
        data.m_deleteCollidingObject = m_deleteCollidingObject;

        return std::move(creationChange);
    }
}
