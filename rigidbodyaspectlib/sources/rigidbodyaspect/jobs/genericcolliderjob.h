#ifndef RIGIDBODYASPECT_GENERICCOLLIDERJOB_H
#define RIGIDBODYASPECT_GENERICCOLLIDERJOB_H

#include "../types.h"

// qt
#include <Qt3DCore/QAspectJob>
#include <QSharedPointer>


namespace rigidbodyaspect
{

  class RigidBodyAspect;

  class GenericColliderJob : public Qt3DCore::QAspectJob {
  public:
    GenericColliderJob(RigidBodyAspect* aspect);

    void setFrameTimeDt(seconds_type dt);

  private:
    RigidBodyAspect* m_aspect;
    seconds_type     m_dt;
    int m_deletedSpheres = 0;
//    int m_total_collisions;

    // QAspectJob interface
  public:
    void run() override;
  };

  using GenericColliderJobPtr = QSharedPointer<GenericColliderJob>;

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_GENERICCOLLIDERJOB_H
