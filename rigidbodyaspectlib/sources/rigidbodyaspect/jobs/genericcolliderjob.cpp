#include "genericcolliderjob.h"

#include "../constants.h"
#include "../backend/rigidbodyaspect.h"
#include "../backend/environmentbackend.h"
#include "../algorithms/simulation.h"
#include "../algorithms/collision.h"
#include "../algorithms/updateproperties.h"
#include "../algorithms/impactresponse.h"

// qt
#include <QDebug>

#include <chrono>
#include <set>
#include <unordered_set>

using namespace std::chrono_literals;

namespace rigidbodyaspect
{

using Sphere = rbtypes::Sphere;
using Plane = rbtypes::FixedPlane;
using Cylinder = rbtypes::FixedCylinder;

using DynamicObjects = std::vector<Sphere*>;
using FixedObjects = std::vector<Plane*>;
using FixedCylinders = std::vector<Cylinder*>;
using CollisionContainer = algorithms::collision::CollisionContainer;

  GenericColliderJob::GenericColliderJob(RigidBodyAspect* aspect)
      : m_aspect{aspect}/*, m_total_collisions{0}*/
  {
  }

  void GenericColliderJob::setFrameTimeDt(seconds_type dt) { m_dt = dt; }

  void GenericColliderJob::run()
  {
      // Init
      auto& rigid_bodies = m_aspect->rigidBodies();
      DynamicObjects dynamicObjects;
      for(auto& d_obj : rigid_bodies.spheres()) {
          dynamicObjects.push_back(&d_obj);
      }

      FixedObjects fixedObjects;
      for(auto& s_obj : rigid_bodies.fixedPlanes()) {
          fixedObjects.push_back(&s_obj);
      }

      FixedCylinders fixedCylinders;
      for(auto& fc_obj : rigid_bodies.cylinders()) {
          fixedCylinders.push_back(&fc_obj);
      }

      CollisionContainer collisions;

      // Compute cache properties
      for (auto& d_obj : dynamicObjects) {
        if(d_obj->m_state == SphereStates::AtRestState) continue;

        d_obj->m_tc = 0ms;
        algorithms::properties::computeCacheProperties(*(m_aspect->environmentBackend(d_obj->m_env_id)), *d_obj, m_dt);
      }

      // Detect collisions
      for(auto di = dynamicObjects.begin(); di != dynamicObjects.end(); ++di) {
//          if((*di)->m_state == SphereStates::AtRestState) continue;

          // Detect collisions between sphere and sphere, and sphere and plane
          auto detected_collisions = algorithms::collision::detectCollisions(m_dt, **di, di+1,
                                             dynamicObjects.end(), fixedObjects);
          if(detected_collisions.size())
              collisions.insert(collisions.end(), detected_collisions.begin(), detected_collisions.end());

          // Detect collisions between sphere and cylinder
          auto cylinder_collisions = algorithms::collision::detectCollisions(m_dt, **di, fixedCylinders);

          if(cylinder_collisions.size())
              collisions.insert(collisions.end(), cylinder_collisions.begin(), cylinder_collisions.end());
      }

      // Sort collision objects and make unique
      algorithms::collision::sortAndMakeUnique(collisions);

//      if(collisions.size() == 2) {
//          std::cout << collisions.size() << std::endl;
//      }

      // Handle collisions
      while(collisions.size()) {
//          m_total_collisions++;
//          std::cout << m_total_collisions << std::endl;
          auto collision = collisions.front();
          collisions.pop_front();

          Sphere* obj0 = collision.m_obj0;
          Sphere* obj1 = nullptr;
          if(std::holds_alternative<Sphere*>(collision.m_obj1))
              obj1 = std::get<Sphere*>(collision.m_obj1);

          // Simulate moving objects to t
          algorithms::simulation::simulateDsByFactor(*obj0, collision.m_t.count() / (m_dt.count() - obj0->m_tc.count()));
          obj0->m_tc = collision.m_t;
          if(obj1) {
              algorithms::simulation::simulateDsByFactor(*obj1, collision.m_t.count() / (m_dt.count() - obj1->m_tc.count()));
              obj1->m_tc = collision.m_t;
          }

          // Update velocity
          algorithms::properties::updateVelocity(*(m_aspect->environmentBackend(obj0->m_env_id)), *obj0, m_dt, collision.m_x);
          if(obj1) {
              algorithms::properties::updateVelocity(*(m_aspect->environmentBackend(obj1->m_env_id)), *obj1, m_dt, collision.m_x);
          }

          // Compute impact response
          if(std::holds_alternative<Plane*>(collision.m_obj1)) {
              algorithms::impactresponse::computeImpact(*obj0, *std::get<Plane*>(collision.m_obj1));
          }
          else if(std::holds_alternative<Cylinder*>(collision.m_obj1)) {
              algorithms::impactresponse::computeImpact(*obj0, *std::get<Cylinder*>(collision.m_obj1));
          }
          else if(obj1) {
              algorithms::impactresponse::computeImpact(*obj0, *obj1);
          }

          // Compute updated cache properties
          algorithms::properties::computeCacheProperties(*(m_aspect->environmentBackend(obj0->m_env_id)),
                                                       *obj0, m_dt - obj0->m_tc);
          if(collision.m_obj1_moving) {
              algorithms::properties::computeCacheProperties(*(m_aspect->environmentBackend(obj1->m_env_id)), *obj1, m_dt - obj1->m_tc);
          }
          else if(std::holds_alternative<Plane*>(collision.m_obj1)) {
              algorithms::properties::checkState(*obj0, *std::get<Plane*>(collision.m_obj1));
          }

          // Inner loop (Check for new collisions);
          auto newCollisions = algorithms::collision::detectNewCollisions(collision, m_dt, dynamicObjects, fixedObjects, fixedCylinders);
          if(newCollisions.size()) {
              collisions.insert(collisions.end(), newCollisions.begin(), newCollisions.end());
          }

          // Sort collisions and make unique
          algorithms::collision::sortAndMakeUnique(collisions);
      }


      // Simulate moving bodies
      for(auto& d_obj : dynamicObjects) {
          if(d_obj->m_state == SphereStates::MarkForDeletion) {
              d_obj->setFrameParent(GM2Vector{1, 0, 0}, GM2Vector{0, 1, 0}, GM2Vector{-30, 10, 0});
              d_obj->m_state = SphereStates::AtRestState;
              ++m_deletedSpheres;
              d_obj->translateLocal(GM2Vector{0, 0, m_deletedSpheres * 2.0});
              continue;
          }
          else if(d_obj->m_state == SphereStates::AtRestState) continue;

          algorithms::simulation::simulateMovingBody(*d_obj);
          const auto tr = m_dt - d_obj->m_tc;
          algorithms::properties::updateVelocity(*(m_aspect->environmentBackend(d_obj->m_env_id)), *d_obj, m_dt, tr.count() / m_dt.count());
      }
  }

}   // namespace rigidbodyaspect
