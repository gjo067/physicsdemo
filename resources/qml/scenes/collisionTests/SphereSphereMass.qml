import QtQuick 2.0

import Qt3D.Core 2.0

import com.uit.GMlib2Qt 1.0
import com.uit.STE6245 1.0
import com.uit.STE6245.RigidBody 1.0 as RB

import "../../objects" as Objects

SceneObject {
    property alias ball : ball.ball_item
    property alias rba_environment : rba_environment
    property alias rba_environment2 : rba_environment2
    property alias ball_rbc: ball.ball_item_rbc

    RB.Environment {
      id: rba_environment
      gravity: Qt.vector3d(0,0,0)
    }

    RB.Environment {
      id: rba_environment2
      gravity: Qt.vector3d(0,0,0)
    }

    Ground {
      id: ground

      defaultMesh.samples: Qt.size(2,2)

      RB.PlaneController{
        id: ground_rbc

        pt: ground.pt
        u: ground.u
        v: ground.v

        environment: rba_environment
        dynamicsType: RB.RigidBodyNS.StaticObject
      }

      Component.onCompleted: {

        // place object
        setParametersQt(Qt.vector3d(-20, 0, 20), Qt.vector3d(40, 0, 0), Qt.vector3d(0, 0, -40))

        // reset plane RB-controller
        ground_rbc.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
      }
    }


    Objects.BallItem {
        id: ball

        ball_item.radius: 0.3
        ball_item_rbc.mass: 1
        ball_item_rbc.velocity: Qt.vector3d(0, 0, 0)

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(0, 12, 0))
        }
    }

    Objects.BallItem {
        id: ball2

        ball_item.radius: 1.2
        ball_item_rbc.mass: 10000
        ball_item_rbc.velocity: Qt.vector3d(0, -0.5, 0)

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(0, 15, 0))
        }
    }
}
