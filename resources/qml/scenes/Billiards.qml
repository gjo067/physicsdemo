import QtQuick 2.0

import Qt3D.Core 2.0

import com.uit.GMlib2Qt 1.0
import com.uit.STE6245 1.0
import com.uit.STE6245.RigidBody 1.0 as RB

import "../objects" as Objects

SceneObject {
    property alias ball : ball.ball_item
    property alias rba_environment : rba_environment
    property alias rba_environment2 : rba_environment2
    property alias ball_rbc: ball.ball_item_rbc

    RB.Environment {
      id: rba_environment
      gravity: Qt.vector3d(0,-10,0)
    }

    RB.Environment {
      id: rba_environment2
      gravity: Qt.vector3d(0,0,0)
    }

    Objects.Cylinder {
        cylinder_rbc.deleteCollidingObject: true
        Component.onCompleted: {
            cylinder.translateGlobalQt(Qt.vector3d(-24.5, 5, 0))
            cylinder.rotateGlobalQt(1.57, Qt.vector3d(1, 0, 0))
        }
    }

    Objects.Cylinder {
        cylinder_rbc.deleteCollidingObject: true
        Component.onCompleted: {
            cylinder.translateGlobalQt(Qt.vector3d(24.5, 5, 0))
            cylinder.rotateGlobalQt(1.57, Qt.vector3d(1, 0, 0))
        }
    }

    Objects.Cylinder {
        cylinder_rbc.deleteCollidingObject: true
        Component.onCompleted: {
            cylinder.translateGlobalQt(Qt.vector3d(-24.5, 5, 49.5))
            cylinder.rotateGlobalQt(1.57, Qt.vector3d(1, 0, 0))
        }
    }

    Objects.Cylinder {
        cylinder_rbc.deleteCollidingObject: true
        Component.onCompleted: {
            cylinder.translateGlobalQt(Qt.vector3d(24.5, 5, 49.5))
            cylinder.rotateGlobalQt(1.57, Qt.vector3d(1, 0, 0))
        }
    }

    Objects.Cylinder {
        cylinder_rbc.deleteCollidingObject: true
        Component.onCompleted: {
            cylinder.translateGlobalQt(Qt.vector3d(-24.5, 5, -49.5))
            cylinder.rotateGlobalQt(1.57, Qt.vector3d(1, 0, 0))
        }
    }

    Objects.Cylinder {
        cylinder_rbc.deleteCollidingObject: true
        Component.onCompleted: {
            cylinder.translateGlobalQt(Qt.vector3d(24.5, 5, -49.5))
            cylinder.rotateGlobalQt(1.57, Qt.vector3d(1, 0, 0))
        }
    }

    Objects.Plane {
        plane.defaultMaterial.diffuse: "darkGreen"
        plane.defaultMaterial.shininess: 0.5
        plane.defaultMaterial.alpha: 1
        plane.defaultMaterial.cool: "darkGreen"
        plane.defaultMaterial.beta: 0
        plane.defaultMaterial.warm: "darkGreen"

        plane_rbc.friction: 1
        plane_rbc.restitution: 0.9

        Component.onCompleted: {
            plane.setParametersQt(Qt.vector3d(-25, 0, 50), Qt.vector3d(50, 0, 0), Qt.vector3d(0, 0, -100))
        }
    }

    Objects.Plane {
        plane.defaultMaterial.diffuse: "darkGreen"
        plane.defaultMaterial.shininess: 1
        plane.defaultMaterial.alpha: 1
        plane.defaultMaterial.cool: "darkGreen"
        plane.defaultMaterial.beta: 0
        plane.defaultMaterial.warm: "darkGreen"

        plane_rbc.friction: 1

        Component.onCompleted: {
            plane.setParametersQt(Qt.vector3d(25, 0, 50), Qt.vector3d(0, 5, 0), Qt.vector3d(0, 0, -100))
        }
    }

    Objects.Plane {
        plane.defaultMaterial.diffuse: "darkGreen"
        plane.defaultMaterial.shininess: 1
        plane.defaultMaterial.alpha: 1
        plane.defaultMaterial.cool: "darkGreen"
        plane.defaultMaterial.beta: 0
        plane.defaultMaterial.warm: "darkGreen"

        plane_rbc.friction: 1

        Component.onCompleted: {
            plane.setParametersQt(Qt.vector3d(-25, 0, -50), Qt.vector3d(0, 5, 0), Qt.vector3d(0, 0, 100))
        }
    }

    Objects.Plane {
        plane.defaultMaterial.diffuse: "darkGreen"
        plane.defaultMaterial.shininess: 1
        plane.defaultMaterial.alpha: 1
        plane.defaultMaterial.cool: "darkGreen"
        plane.defaultMaterial.beta: 0
        plane.defaultMaterial.warm: "darkGreen"

        plane_rbc.friction: 1

        Component.onCompleted: {
            plane.setParametersQt(Qt.vector3d(-25, 0, -50), Qt.vector3d(50, 0, 0), Qt.vector3d(0, 5, 0))
        }
    }

    Objects.Plane {
        plane.defaultMaterial.diffuse: "darkGreen"
        plane.defaultMaterial.shininess: 1
        plane.defaultMaterial.alpha: 1
        plane.defaultMaterial.cool: "darkGreen"
        plane.defaultMaterial.beta: 0
        plane.defaultMaterial.warm: "darkGreen"

        plane_rbc.friction: 1
        Component.onCompleted: {
            plane.setParametersQt(Qt.vector3d(25, 0, 50), Qt.vector3d(-50, 0, 0), Qt.vector3d(0, 5, 0))
        }
    }

    Objects.BallItem {
        id: ball

        ball_item.defaultMaterial.diffuse: "white"
        ball_item.defaultMaterial.warm: "white"

        ball_item.radius: 1
        ball_item_rbc.mass: 1.2
        ball_item_rbc.velocity: Qt.vector3d(0, 0, -50 - Math.random() * 50)

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(-0.1 + Math.random() * 0.2, 1.01, 35))
        }
    }

    Objects.BallItem {
        ball_item.defaultMaterial.diffuse: "#ffed00"
        ball_item.defaultMaterial.warm: "#ffed00"

        ball_item.radius: 1
        ball_item_rbc.mass: 1
        ball_item_rbc.velocity: Qt.vector3d(0, 0, 0)

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(0, 1.01, -35))
        }
    }

    Objects.BallItem {
        ball_item.defaultMaterial.diffuse: "#e62e00"
        ball_item.defaultMaterial.warm: "#e62e00"

        ball_item.radius: 1
        ball_item_rbc.mass: 1
        ball_item_rbc.velocity: Qt.vector3d(0, 0, 0)

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(-1.01, 1.01, -37.01))
        }
    }

    Objects.BallItem {
        ball_item.defaultMaterial.diffuse: "#4d0f00"
        ball_item.defaultMaterial.warm: "#4d0f00"

        ball_item.radius: 1
        ball_item_rbc.mass: 1
        ball_item_rbc.velocity: Qt.vector3d(0, 0, 0)

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(1.01, 1.01, -37.01))
        }
    }

    Objects.BallItem {
        ball_item.defaultMaterial.diffuse: "black"
        ball_item.defaultMaterial.warm: "black"

        ball_item.radius: 1
        ball_item_rbc.mass: 1
        ball_item_rbc.velocity: Qt.vector3d(0, 0, 0)

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(0, 1.01, -39.02))
        }
    }

    Objects.BallItem {
        ball_item.defaultMaterial.diffuse: "#4ce600"
        ball_item.defaultMaterial.warm: "#4ce600"

        ball_item.radius: 1
        ball_item_rbc.mass: 1
        ball_item_rbc.velocity: Qt.vector3d(0, 0, 0)

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(-2.01, 1.01, -39.02))
        }
    }

    Objects.BallItem {
        ball_item.defaultMaterial.diffuse: "#33ff77"
        ball_item.defaultMaterial.warm: "#33ff77"

        ball_item.radius: 1
        ball_item_rbc.mass: 1
        ball_item_rbc.velocity: Qt.vector3d(0, 0, 0)

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(2.01, 1.01, -39.02))
        }
    }

    Objects.BallItem {
        ball_item.defaultMaterial.diffuse: "#990099"
        ball_item.defaultMaterial.warm: "#990099"

        ball_item.radius: 1
        ball_item_rbc.mass: 1
        ball_item_rbc.velocity: Qt.vector3d(0, 0, 0)

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(1.01, 1.01, -41.03))
        }
    }

    Objects.BallItem {
        ball_item.defaultMaterial.diffuse: "#7300e6"
        ball_item.defaultMaterial.warm: "#7300e6"

        ball_item.radius: 1
        ball_item_rbc.mass: 1
        ball_item_rbc.velocity: Qt.vector3d(0, 0, 0)

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(-1.01, 1.01, -41.03))
        }
    }

    Objects.BallItem {
        ball_item.defaultMaterial.diffuse: "#807700"
        ball_item.defaultMaterial.warm: "#807700"

        ball_item.radius: 1
        ball_item_rbc.mass: 1
        ball_item_rbc.velocity: Qt.vector3d(0, 0, 0)

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(3.02, 1.01, -41.03))
        }
    }

    Objects.BallItem {
        ball_item.defaultMaterial.diffuse: "#ff8000"
        ball_item.defaultMaterial.warm: "#ff8000"

        ball_item.radius: 1
        ball_item_rbc.mass: 1
        ball_item_rbc.velocity: Qt.vector3d(0, 0, 0)

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(-3.02, 1.01, -41.03))
        }
    }



    Objects.BallItem {
        ball_item.defaultMaterial.diffuse: "darkBlue"
        ball_item.defaultMaterial.warm: "darkBlue"

        ball_item.radius: 1
        ball_item_rbc.mass: 1
        ball_item_rbc.velocity: Qt.vector3d(0, 0, 0)

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(0, 1.01, -43.04))
        }
    }



    Objects.BallItem {
        ball_item.defaultMaterial.diffuse: "#3399ff"
        ball_item.defaultMaterial.warm: "#3399ff"

        ball_item.radius: 1
        ball_item_rbc.mass: 1
        ball_item_rbc.velocity: Qt.vector3d(0, 0, 0)

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(2.01, 1.01, -43.04))
        }
    }



    Objects.BallItem {
        ball_item.defaultMaterial.diffuse: "#994d00"
        ball_item.defaultMaterial.warm: "#994d00"

        ball_item.radius: 1
        ball_item_rbc.mass: 1
        ball_item_rbc.velocity: Qt.vector3d(0, 0, 0)

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(-2.01, 1.01, -43.04))
        }
    }



    Objects.BallItem {
        ball_item.defaultMaterial.diffuse: "#ff4dff"
        ball_item.defaultMaterial.warm: "#ff4dff"

        ball_item.radius: 1
        ball_item_rbc.mass: 1
        ball_item_rbc.velocity: Qt.vector3d(0, 0, 0)

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(-4.02, 1.01, -43.04))
        }
    }

    Objects.BallItem {
        ball_item.defaultMaterial.diffuse: "#4d0099"
        ball_item.defaultMaterial.warm: "#4d0099"

        ball_item.radius: 1
        ball_item_rbc.mass: 1
        ball_item_rbc.velocity: Qt.vector3d(0, 0, 0)

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(4.02, 1.01, -43.04))
        }
    }
}
