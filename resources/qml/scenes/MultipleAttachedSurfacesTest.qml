import QtQuick 2.0

import Qt3D.Core 2.0

import com.uit.GMlib2Qt 1.0
import com.uit.STE6245 1.0
import com.uit.STE6245.RigidBody 1.0 as RB

import "../objects" as Objects

SceneObject {
    property alias ball : ball.ball_item
    property alias rba_environment : rba_environment
    property alias rba_environment2 : rba_environment2
    property alias ball_rbc: ball.ball_item_rbc

    RB.Environment {
      id: rba_environment
      gravity: Qt.vector3d(0,-10,0)
    }

    RB.Environment {
      id: rba_environment2
      gravity: Qt.vector3d(0,0,0)
    }

    Objects.Plane {
        plane_rbc.friction: 1
        plane_rbc.restitution: 0.9;

        Component.onCompleted: {
            plane.setParametersQt(Qt.vector3d(0, 0, 0), Qt.vector3d(120, -40, 0), Qt.vector3d(0, 25, -40))
        }
    }

    Objects.Plane {
        plane_rbc.friction: 1
        plane_rbc.restitution: 0.9;

        Component.onCompleted: {
            plane.setParametersQt(Qt.vector3d(0, 0, 0), Qt.vector3d(0, 25, 40), Qt.vector3d(120, -40, 0))
        }
    }

    Objects.Plane {
        plane_rbc.friction: 1
        plane_rbc.restitution: 0.9;

        Component.onCompleted: {
            plane.setParametersQt(Qt.vector3d(110, -40, 40), Qt.vector3d(10, 80, 0), Qt.vector3d(0, 0, -80))
        }
    }

    Objects.BallItem {
        id: ball

        ball_item.radius: 1
        ball_item_rbc.mass: 1
        ball_item_rbc.velocity: Qt.vector3d(10, 0, 0)

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(0, 3.01, 1))
        }
    }
    Objects.BallItem {
        ball_item.radius: 1
        ball_item_rbc.mass: 1
        ball_item_rbc.velocity: Qt.vector3d(5, 0, 0)

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(0, 3.01, -1))
        }
    }
}
