import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

import Qt3D.Core 2.10
import Qt3D.Render 2.10
import Qt3D.Input 2.0
import Qt3D.Animation 2.10
import Qt3D.Extras 2.10

import com.uit.GMlib2Qt 1.0
import com.uit.STE6245 1.0
import com.uit.STE6245.RigidBody 1.0 as RB

import "objects" as Obj
import "scenes" as Scenes


ApplicationWindow {
  id: root

  function updateGravity() {
    var g = Qt.vector3d(g_x.value,g_y.value,g_z.value)
    rba_environment.gravity = g
  }

  visible: true

  width: 800
  height: 600


  menuBar: MenuBar {
    Menu {
      title: "File"
      MenuSeparator{}
      MenuItem { text: "Quit"; onTriggered: Qt.quit() }
    }
    Menu {
        title: "Scenes"
        MenuItem {
            text: "Billiards"
            onTriggered: {
                scene_loader.source = "scenes/Billiards.qml";
                scene_loader.reload();
            }
        }

        MenuSeparator {}

        Menu {
            title: "Collision Tests"
            MenuItem {
                text: "Sphere Plane"
                onTriggered: {
                    scene_loader.source = "scenes/collisionTests/SpherePlane.qml";
                    scene_loader.reload();
                }
            }

            MenuItem {
                text: "Sphere Sphere"
                onTriggered: {
                    scene_loader.source = "scenes/collisionTests/SphereSphere.qml";
                    scene_loader.reload();
                }
            }

            MenuItem {
                text: "Sphere Cylinder"
                onTriggered: {
                    scene_loader.source = "scenes/collisionTests/CylinderSphere.qml";
                    scene_loader.reload();
                }
            }

            MenuItem {
                text: "Sphere In Cylinder"
                onTriggered: {
                    scene_loader.source = "scenes/collisionTests/SphereInCylinder.qml";
                    scene_loader.reload();
                }
            }

            MenuItem {
                text: "Sphere Sphere Different Mass"
                onTriggered: {
                    scene_loader.source = "scenes/collisionTests/SphereSphereMass.qml";
                    scene_loader.reload();
                }
            }

            MenuItem {
                text: "Sliding Sphere Plane"
                onTriggered: {
                    scene_loader.source = "scenes/collisionTests/SlidingSpherePlane.qml";
                    scene_loader.reload();
                }
            }

            MenuItem {
                text: "Sliding Sphere Sphere"
                onTriggered: {
                    scene_loader.source = "scenes/collisionTests/SlidingSphereSphere.qml";
                    scene_loader.reload();
                }
            }

            MenuItem {
                text: "Sphere On Resting Sphere"
                onTriggered: {
                    scene_loader.source = "scenes/collisionTests/SphereOnRestingSphere.qml";
                    scene_loader.reload();
                }
            }

            MenuItem {
                text: "MultipleSpheres"
                onTriggered: {
                    scene_loader.source = "scenes/collisionTests/MultipleSphereCol.qml";
                    scene_loader.reload();
                }
            }
        }

        Menu {
            title: "State Change Tests"

            MenuItem {
                text: "Free Rest"
                onTriggered: {
                    scene_loader.source = "scenes/stateChangesTests/FreeRest.qml";
                    scene_loader.reload();
                }
            }

            MenuItem {
                text: "Slide Free"
                onTriggered: {
                    scene_loader.source = "scenes/stateChangesTests/SlideFree.qml";
                    scene_loader.reload();
                }
            }

            MenuItem {
                text: "FreeSlideRest"
                onTriggered: {
                    scene_loader.source = "scenes/stateChangesTests/FreeSlideRest.qml";
                    scene_loader.reload();
                }
            }
        }

        MenuItem {
            text: "Inner Loop"
            onTriggered: {
                scene_loader.source = "scenes/InnerLoopTest.qml";
                scene_loader.reload();
            }
        }

        MenuItem {
            text: "Multiple Attached Surfaces Test"
            onTriggered: {
                scene_loader.source = "scenes/MultipleAttachedSurfacesTest.qml";
                scene_loader.reload();
            }
        }
    }
  }


  statusBar: Item {
    anchors.bottom: parent.bottom
    anchors.left: parent.left
    anchors.right: parent.right
    height: 40

    RowLayout {
      anchors.fill: parent
      anchors.leftMargin: 5
      anchors.rightMargin: 5

      Button {
          text: "Toggle simulation"
          onClicked: { scenario.simulatorRunStatus = !scenario.simulatorRunStatus; }
      }

      Button {
          text: "Reload"
          onClicked: { scene_loader.reload(); }
      }

      Item { Layout.fillWidth: true }
      Item{ width: 10 }
      Text{ text: "Gravity" }
      SpinBox{ id:g_x; decimals: 2; stepSize: 0.05; minimumValue: -20; maximumValue: 20; value: 0; onValueChanged: root.updateGravity() }
      SpinBox{ id:g_y; decimals: 2; stepSize: 0.05; minimumValue: -20; maximumValue: 20; value: -9.80; onValueChanged: root.updateGravity()  }
      SpinBox{ id:g_z; decimals: 2; stepSize: 0.05; minimumValue: -20; maximumValue: 20; value: 0; onValueChanged: root.updateGravity()  }
    }

  }

  Scenario {
    id: scenario
    anchors.fill: parent

    EntityLoader {
        id: scene_loader

        source: "scenes/Billiards.qml"

        onStatusChanged: {
            if(status === EntityLoader.Ready )
                performOnLoadStuff()
        }

        function reload() {
            var reload_source = source
            source = ""
            source = reload_source
        }

        function performOnLoadStuff() {
            // do on-load stuff
        }
    }
  }
}
