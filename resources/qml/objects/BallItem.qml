import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

import Qt3D.Core 2.10
import Qt3D.Render 2.10
import Qt3D.Input 2.0
import Qt3D.Animation 2.10
import Qt3D.Extras 2.10

import com.uit.GMlib2Qt 1.0
import com.uit.STE6245 1.0
import com.uit.STE6245.RigidBody 1.0 as RB

SceneObject {
    property alias ball_item: ball_item
    property alias ball_item_rbc: ball_item_rbc

    Ball {
      id: ball_item

      radius: 1

      defaultMesh.samples: Qt.size(20,20)

      onRadiusChanged: defaultMesh.reSample()

      RB.SphereController{
        id: ball_item_rbc

        environment: rba_environment
        dynamicsType: RB.RigidBodyNS.DynamicObject

        radius: ball_item.radius
        velocity: Qt.vector3d(0, 10, 0);

        onFrameComputed: ball_item.setFrameParentQt(dir,up,pos);
      }

      Component.onCompleted: {

        // initial object placement
        //translateGlobalQt( Qt.vector3d(-3,10,0) )

        // reset sphere RB-controller
        ball_item_rbc.resetFrameByDup( directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt() )
      }
    }
}
