import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

import Qt3D.Core 2.10
import Qt3D.Render 2.10
import Qt3D.Extras 2.10

import com.uit.GMlib2Qt 1.0
import com.uit.STE6245 1.0
import com.uit.STE6245.RigidBody 1.0 as RB

SceneObject {
    property alias plane: plane
    property alias plane_rbc: plane_rbc

    Ground {
      id: plane

      defaultMesh.samples: Qt.size(2,2)

      RB.PlaneController{
        id: plane_rbc

        pt: plane.pt
        u: plane.u
        v: plane.v

        environment: rba_environment
        dynamicsType: RB.RigidBodyNS.StaticObject
      }

      Component.onCompleted: {

        // reset plane RB-controller
        plane_rbc.resetFrameByDup( directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt() )
      }
    }
}
