import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

import Qt3D.Core 2.10
import Qt3D.Render 2.10
import Qt3D.Input 2.0
import Qt3D.Animation 2.10
import Qt3D.Extras 2.10

import com.uit.GMlib2Qt 1.0
import com.uit.STE6245 1.0
import com.uit.STE6245.RigidBody 1.0 as RB

SceneObject {
    property alias cylinder: cylinder
    property alias cylinder_rbc: cylinder_rbc

    Cylinder {
        id: cylinder

        radius: 1
        height: 5

        defaultMesh.samples: Qt.size(20, 20)

        onRadiusChanged: defaultMesh.reSample()
        onHeightChanged: defaultMesh.reSample()

        RB.CylinderController {
            id: cylinder_rbc

            radius: cylinder.radius
            height: cylinder.height

            environment: rba_environment
            dynamicsType: RB.RigidBodyNS.StaticObject
        }

        Component.onCompleted: {
            cylinder_rbc.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
        }
    }
}
