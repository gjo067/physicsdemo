import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

import Qt3D.Core 2.10
import Qt3D.Render 2.10
import Qt3D.Input 2.0
import Qt3D.Animation 2.10
import Qt3D.Extras 2.10

import com.uit.GMlib2Qt 1.0
import com.uit.STE6245 1.0
import com.uit.STE6245.RigidBody 1.0 as RB

import "../objects" as Objects

SceneObject {

    Objects.BallItem {
        ball_item.radius: 0.5

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(-9, 10, 0))
        }
    }

    Objects.BallItem {
        ball_item.radius: 0.5

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(-7, 9, 0))
        }
    }

    Objects.BallItem {
        ball_item.radius: 0.5

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(-5, 8, 0))
        }
    }

    Objects.BallItem {
        ball_item.radius: 0.5

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(-3, 7, 0))
        }
    }

    Objects.BallItem {
        ball_item.radius: 0.5

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(-1, 6, 0))
        }
    }

    Objects.BallItem {
        ball_item.radius: 0.5

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(1, 5, 0))
        }
    }

    Objects.BallItem {
        ball_item.radius: 0.5

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(3, 4, 0))
        }
    }

    Objects.BallItem {
        ball_item.radius: 0.5

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(5, 3, 0))
        }
    }

    Objects.BallItem {
        ball_item.radius: 0.5

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(7, 2, 0))
        }
    }

    Objects.BallItem {
        ball_item.radius: 0.5

        Component.onCompleted: {
            ball_item.translateGlobalQt(Qt.vector3d(9, 1, 0))
        }
    }
}
