import Qt3D.Core 2.0
import com.uit.GMlib2Qt 1.0

SceneObject {
    PPlane {
        defaultMesh.samples: Qt.size(20, 20)
        components: [
            Transform {
                translation: Qt.vector3d(-10, 0, 0)
            }
        ]
    }

    PSphere {
        defaultMesh.samples: Qt.size(20, 20)
        radius: 4
        components: [
            Transform {
                translation: Qt.vector3d(5, -5, 0)
            }
        ]
    }
}
