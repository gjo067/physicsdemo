import QtQuick 2.0
import QtQuick.Layouts 1.3

ComponentDelegate {
  title: "Mesh"

  GridLayout {
    columns: 3

    anchors.left: parent.left
    anchors.right: parent.right

    // Samples u
    Text { id: samples_u_text; text: "Samples U" }
    Item{Layout.fillWidth: true}
    Text { id: samples_u_value; text: component_object.samples.width}

    // Samples v
    Text { id: samples_v_text; text: "Samples V" }
    Item{Layout.fillWidth: true}
    Text { id: samples_v_value; text: component_object.samples.height}
  }

}
