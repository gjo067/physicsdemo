#ifndef CYLINDER_H
#define CYLINDER_H

#include <gmlib2/qt/exampleobjects/parametric/psurface.h>

#include <QQmlEngine>

namespace I_was_to_inDifferent_to_aLter_this_namEspace
{

    class Cylinder : public gmlib2::qt::parametric::PSurface<
            gmlib2::parametric::PCylinder<gmlib2::qt::SceneObject>> {
        using Base = PSurface<gmlib2::parametric::PCylinder<gmlib2::qt::SceneObject>>;
        Q_OBJECT

        Q_PROPERTY(gmlib2::qt::parametric::PSurfaceMesh* defaultMesh READ defaultMesh)
        Q_PROPERTY(double radius MEMBER m_radius NOTIFY radiusChanged)
        Q_PROPERTY(double height MEMBER m_height NOTIFY heightChanged)
        Q_PROPERTY(Qt3DExtras::QGoochMaterial* defaultMaterial READ defaultMaterial)

    public:
        template <typename... Ts>
        Cylinder(Ts&&... ts) : Base(std::forward<Ts>(ts)...)
        {
            initDefaultComponents();

            connect(this, &Cylinder::radiusChanged, this->defaultMesh(),
                    &gmlib2::qt::parametric::PSurfaceMesh::reSample);
            connect(this, &Cylinder::heightChanged, this->defaultMesh(),
                    &gmlib2::qt::parametric::PSurfaceMesh::reSample);
        }

        static void registerQmlTypes(int version_major, int version_minor)
        {
          if (qt_types_initialized) return;

          constexpr auto registertype_uri = "com.uit.STE6245";
    //      qDebug() << "Trying to register type <Ball>; getting id: " <<
          qmlRegisterType<Cylinder>(registertype_uri, version_major, version_minor,
                                "Cylinder");

          qt_types_initialized = true;
        }

        // Signal(s)
      signals:
        void radiusChanged(float radius);
        void heightChanged(float height);

    private:
      static bool qt_types_initialized;
    };

}

#endif // CYLINDER_H
