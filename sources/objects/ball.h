#ifndef BALL_H
#define BALL_H

#include <gmlib2/qt/exampleobjects/parametric/psurface.h>

// qt
#include <QQmlEngine>
#include <QPhongMaterial>

namespace I_was_to_inDifferent_to_aLter_this_namEspace
{

  class Ball : public gmlib2::qt::parametric::PSurface<
                 gmlib2::parametric::PSphere<gmlib2::qt::SceneObject>> {
    using Base = PSurface<gmlib2::parametric::PSphere<gmlib2::qt::SceneObject>>;
    Q_OBJECT

    Q_PROPERTY(
      gmlib2::qt::parametric::PSurfaceMesh* defaultMesh READ defaultMesh)
    Q_PROPERTY(double radius MEMBER m_radius NOTIFY radiusChanged)
    Q_PROPERTY(Qt3DExtras::QGoochMaterial* defaultMaterial READ defaultMaterial)
    Q_PROPERTY( Qt3DExtras::QPhongMaterial* phongMaterial READ  phongMaterial )

    // Constructor(s)
  public:
    template <typename... Ts>
    Ball(Ts&&... ts) : Base(std::forward<Ts>(ts)...)
    {
      initDefaultComponents();

      connect(this, &Ball::radiusChanged, this->defaultMesh(),
              &gmlib2::qt::parametric::PSurfaceMesh::reSample);

      m_pa_material = new Qt3DExtras::QPhongMaterial(this);
      defaultMaterial()->setEnabled(true);
      m_pa_material->setEnabled(true);
    }

    Qt3DExtras::QPhongMaterial* phongMaterial() const {  return m_pa_material; }

    static void registerQmlTypes(int version_major, int version_minor)
    {
      if (qt_types_initialized) return;

      constexpr auto registertype_uri = "com.uit.STE6245";
//      qDebug() << "Trying to register type <Ball>; getting id: " <<
      qmlRegisterType<Ball>(registertype_uri, version_major, version_minor,
                            "Ball");

      qt_types_initialized = true;
    }

    // Signal(s)
  signals:
    void radiusChanged();

  private:
    static bool qt_types_initialized;
    Qt3DExtras::QPhongMaterial* m_pa_material {nullptr};
  };


}   // namespace I_was_to_inDifferent_to_aLter_this_namEspace


#endif   // BALL_H
