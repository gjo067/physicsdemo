#ifndef LINE_H
#define LINE_H

#include <gmlib2/qt/exampleobjects/parametric/curves/pline.h>

#include <QQmlEngine>

namespace I_was_to_inDifferent_to_aLter_this_namEspace {

    class Line : public gmlib2::qt::parametric::PCurve<
            gmlib2::parametric::PLine<gmlib2::qt::SceneObject>> {
        using Base = PCurve<gmlib2::parametric::PLine<gmlib2::qt::SceneObject>>;
        Q_OBJECT

        Q_PROPERTY(gmlib2::qt::parametric::PCurveMesh* defaultMesh READ defaultMesh)
        Q_PROPERTY(QVector3D pt READ pt WRITE setPt NOTIFY ptChanged)
        Q_PROPERTY(QVector3D v READ v WRITE setV NOTIFY vChanged)

    public:
        // Constructor(s)
        template <typename... Ts>
        Line(Ts&&... ts) : Base(std::forward<Ts>(ts)...) {
            initDefaultComponents();

            connect(this, &Line::ptChanged, this->defaultMesh(),
                    &gmlib2::qt::parametric::PCurveMesh::reSample);
            connect(this, &Line::vChanged, this->defaultMesh(),
                    &gmlib2::qt::parametric::PCurveMesh::reSample);
        }

        Q_INVOKABLE void setParametersQt(const QVector3D& p, const QVector3D& v) {
            m_pt = VectorH_Type{double(p.x()), double(p.y()), double(p.z()), 1.0};
            m_v = VectorH_Type{double(v.x()), double(v.y()), double(v.z()), 0.0};
            if(defaultMesh()) defaultMesh()->reSample();
            ptChanged(p);
            vChanged(v);
        }

        static void registerQmlTypes(int version_major, int version_minor) {
            if(qt_types_initialized) return;

            constexpr auto registertype_uri = "com.uit.STE6245";
            qmlRegisterType<Line>(registertype_uri, version_major, version_minor, "Line");

            qt_types_initialized = true;
        }

        const QVector3D pt() const;
        const QVector3D v() const;

    public slots:
        void setPt(const QVector3D& pt);
        void setV(const QVector3D& v);

    signals:
        void ptChanged(QVector3D pt);
        void vChanged(QVector3D v);

    private:
        static bool qt_types_initialized;
    };
}

#endif // LINE_H
