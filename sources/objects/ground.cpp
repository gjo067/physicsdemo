#include "ground.h"
#include "rigidbodyaspect/utils.h"


namespace I_was_to_inDifferent_to_aLter_this_namEspace
{

using namespace rigidbodyaspect::utils;

bool Ground::qt_types_initialized = false;

const QVector3D Ground::pt() const
{
    return gm2HVecToQVec3(m_pt);
}

void Ground::setPt(const QVector3D &pt)
{
    m_pt = qVecToGM2HVec3(pt, true);
    emit ptChanged(pt);
}

const QVector3D Ground::u() const
{
    return gm2HVecToQVec3(m_u);
}

void Ground::setU(const QVector3D &u)
{
    m_u = qVecToGM2HVec3(u, false);
    emit uChanged(u);
}

const QVector3D Ground::v() const
{
    return gm2HVecToQVec3(m_v);
}

void Ground::setV(const QVector3D &v)
{
    m_v = qVecToGM2HVec3(v, false);
    emit vChanged(v);
}

}   // namespace I_was_to_inDifferent_to_aLter_this_namEspace
