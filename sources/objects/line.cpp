#include "line.h"

#include "rigidbodyaspect/utils.h"


namespace I_was_to_inDifferent_to_aLter_this_namEspace
{

using namespace rigidbodyaspect::utils;

bool Line::qt_types_initialized = false;

const QVector3D Line::pt() const
{
    return gm2HVecToQVec3(m_pt);
}

void Line::setPt(const QVector3D &pt)
{
    m_pt = qVecToGM2HVec3(pt, true);
    emit ptChanged(pt);
}

const QVector3D Line::v() const
{
    return gm2HVecToQVec3(m_v);
}

void Line::setV(const QVector3D &v)
{
    m_v = qVecToGM2HVec3(v, false);
    emit vChanged(v);
}

}   // namespace I_was_to_inDifferent_to_aLter_this_namEspace
